const navbar = document.querySelector(".navbar");
const sections = document.querySelectorAll("section");
const navbarItems = document.querySelectorAll(".navbar-nav li");

document.addEventListener("scroll", () => {
	window.scrollY > 10
		? navbar.classList.add("scrolled")
		: navbar.classList.remove("scrolled");
});

sections.forEach(element => {
	document.addEventListener("scroll", () => {
		const self = element;

		if (
			window.scrollY >= element.offsetTop &&
			window.scrollY < element.offsetTop + element.offsetHeight
		) {
			navbarItems.forEach(element => {
				if (element.firstElementChild.getAttribute("href") === `#${self.id}`) {
					element.firstElementChild.classList.add("active");
				}
			});
		} else {
			navbarItems.forEach(element => {
				if (element.firstElementChild.getAttribute("href") === `#${self.id}`) {
					element.firstElementChild.classList.remove("active");
				}
			});
		}
	});
});

navbarItems.forEach(element => {
	element.addEventListener("click", event => {
		setTimeout(() => {
			removeActiveItems();
			element.firstElementChild.classList.add("active");
		}, 1);
	});
});

function removeActiveItems() {
	navbarItems.forEach(element => {
		element.firstElementChild.classList.remove("active");
	});
}
