const navbarActivatior = document.querySelector(".navbar__toggle");
const navbarList = document.querySelector(".navbar--list");

/**
 * Mobile navbar activator
 */
navbarActivatior.addEventListener("click", () => {
	navbarList.classList.toggle("open");
});
